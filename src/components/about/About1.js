
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { FiChevronRight, FiPlay } from 'react-icons/fi'
import about_video_img from '../../assets/images/img1.jpg'
import history_img from '../../assets/images/img2.jpg'
import ModalVideo from 'react-modal-video'

export default class About1 extends Component {
    constructor() {
        super()
        this.state = {
            isOpen: false
        }
        this.openModal2 = this.openModal2.bind(this)
    }

    openModal2() {
        this.setState({ isOpen: true })
    }

    render() {
        return (
            <section className="about-st1-wrapper">
                <div className="container">
                    <div className="about-st1-grid">
                        <div className="about-left">
                            <h1 className="about-left-title">
                                We Are Trusted by More Than <strong>100 Clients.</strong>
                            </h1>
                            <p className="about-left-desc">
							Trust is the most valuable business commodity. If customers don't trust you, they won't buy from you. And if you don't sell, you don't survive.                            </p>
                            <Link to="/#" className="theme-button">
                                Join Now <FiChevronRight className="icon" />
                            </Link>
                        </div>
                        <div className="about-right">
                            <img className="about-right-img" src={about_video_img} alt="About Video" />
                            <ModalVideo channel='youtube' isOpen={this.state.isOpen} videoId='t3tsMEhlvwM' onClose={() => this.setState({ isOpen: false })} />
                            <div className="video-button-box">
                                <Link to="/#" className="video-button" onClick={this.openModal2}>
                                    <span className="icon"><FiPlay /></span>
                                    Watch Video
                                </Link>
                            </div>
                        </div>
                    </div>
                    {/* <div className="history-area">
                        <div className="row">
                            <div className="col-8">
                                <img className="history_img" src={history_img} alt="History" />
                            </div>
                            <div className="history-details-box">
                                <h1 className="history-details-title">
                                    More Than <strong>50+  Projects </strong>
                                    Were Completed.
                                </h1>
                                <p className="history-details-desc">
                                    Projects are a major liability for a Managed Service Provider.  They have a significant number of labor hours for the MSP, and a significant investment for the Customer.  Therefore, high visibility within the respective organization.  Any sideways motion or delay can cost either party or both significantly more labor hours, huge financial loss, and disappointment.  To add to this the Resources assigned to the project usually have the highest billable hour rate, are key to providing engineering excellence, and are mentors to the rest of the team. 
                                </p>
                                <Link to="/#" className="theme-button">
                                    Get Started <FiChevronRight className="icon" />
                                </Link>
                            </div>
                        </div>
                    </div> */}
                </div>
            </section>
        )
    }
}
