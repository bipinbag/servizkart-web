import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { FiChevronRight } from 'react-icons/fi'

export default class ServiceCategory extends Component {
    render() {
        return (
            <>
                <div className="service-widgets widget_categories">
                    <h3>Categories.</h3>
                    <ul>
                        <li><Link to="/service-details">Accounting and Ancilliary Services <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Taxation Services<FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Corporate Legal Services <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">HR and Payroll Services <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Learning and Development <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Warehousing and Logistics <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Architect Services <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Strategy and Management <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Valuation and Investment <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Company Law Compliances <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">IT and Software Support <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Sales and Marketing <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Real Estate <FiChevronRight className="icon" /></Link></li>
                    </ul>
                </div>
            </>
        )
    }
}
