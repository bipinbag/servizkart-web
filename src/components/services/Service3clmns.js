import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { FiTarget, FiChevronRight } from "react-icons/fi";
import { FaHandshake } from "react-icons/fa";
import { IoIosNotificationsOutline } from "react-icons/io";


export default class Service3clmns extends Component {
    servicestates = {
        items: [
            {
                icon: <FiTarget className="service-items-icon" />,
                title: 'Our Mission.',
                description: 'sed quia lipsum dolor sit atur adipiscing elit is nunc quis tellus sed ligula porta ultricies quis nec neulla.'
            },
            {
                icon: <IoIosNotificationsOutline className="service-items-icon" />,
                title: 'Our Vision.',
                description: 'sed quia lipsum dolor sit atur adipiscing elit is nunc quis tellus sed ligula porta ultricies quis nec neulla.'
            }
        ]
    }
    render() {
        return (
            <section className="services-area service-3columns">
                <div className="container">
                    <div className="services-items-grid">
                        <div className="service-items">
                            <span className="service-items-num">0</span>
                            <span className="service-items-iconbox"><FiTarget className="service-items-icon" /></span>
                            <Link to="/service-details">
                                <h3 className="service-items-title">'Our Vision</h3>
                            </Link>
                            <p className="service-items-description">SERVIZKART aims to become the biggest outsourcing platform in India. </p>
                            <p className="service-items-description">As a brand, we aim to make SERVIZKART a name synonym for business outsourcing. We aim to make business outsourcing the new normal. We aim to make the process of business outsourcing simple yet effective and efficient in a cost-effective manner.</p>
                            <p className="service-items-description">We at SERVIZKART aim to make business outsourcing available to all business models whether big or small, across nations, industries, and functions.</p>

                        </div>
                        <div className="service-items">
                            <span className="service-items-num">0</span>
                            <span className="service-items-iconbox"><IoIosNotificationsOutline className="service-items-icon" /></span>
                            <Link to="/service-details">
                                <h3 className="service-items-title">'Our Mission</h3>
                            </Link>
                            <p className="service-items-description">SERVIZKART aims to bridge the gap between leading business houses and talented vendors. </p>
                            <p className="service-items-description">We at SERVIZKART aim to connect more and more verified vendors with leading business houses across India as well as globally. We aim to be indispensable among major business networks of start-ups, mid & small scale industries, and large scale industries.</p>
                            <p className="service-items-description">SERVIZKART aims to create a loyal client and vendor base by delivering high-quality, cost-effective and value-added services to our clients changing the face of outsourcing and making it an essential part of business excellence.</p>
                            <p className="service-items-description"></p>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
