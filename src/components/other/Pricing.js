import React, { Component } from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import SectionsTitle from '../common/SectionsTitle'
import { Link } from 'react-router-dom'
import { FiShoppingCart } from 'react-icons/fi'

export default class Pricing extends Component {
    render() {
        return (
            <section className="pricing-wrapper">
                <div className="container">
                    <div className="text-center">
                        <SectionsTitle title="Choose Your Plan." subtitle="Affordable Packages" />
                    </div>
                    <Tabs>
                        <TabList className="tabs-nav">
                            <Tab>Monthly</Tab>
                            <Tab>Yearly</Tab>
                        </TabList>
                        <TabPanel className="tabs-content text-center">
                            <div className="pricing-plan-grid">
                                <div className="pricing-plan-box">
                                    <h2 className="title">Basic</h2>
                                    <h1 className="price"> <sup>INR</sup>1000</h1>
                                    <h3 className="mo">/Month</h3>
                                    <ul className="feature-lists">
                                        <li>Limitless concepts</li>
                                        <li>Community access</li>
                                        <li>Expert Reviews</li>
                                        <li>Basic Listing</li>
                                        <li>Free Support</li>
                                        <li>Free registration</li>
                                    </ul>
                                    <Link to="/#" className="theme-button"><FiShoppingCart className="icon" /> Get Started</Link>
                                </div>
                                <div className="pricing-plan-box active">
                                    <h2 className="title">Popular</h2>
                                    <h1 className="price"> <sup>INR</sup>2000</h1>
                                    <h3 className="mo">/Month</h3>
                                    <ul className="feature-lists">
                                        <li>Limitless concepts</li>
                                        <li>Community access</li>
                                        <li>Expert Reviews</li>
                                        <li>Professional Listing</li>
                                        <li>Direct Connections</li>
                                        <li>24/7 Support</li>
                                    </ul>
                                    <Link to="/#" className="theme-button"><FiShoppingCart className="icon" /> Get Started</Link>
                                </div>
                                <div className="pricing-plan-box">
                                    <h2 className="title">Premium</h2>
                                    <h1 className="price"> <sup>INR</sup>3000</h1>
                                    <h3 className="mo">/Month</h3>
                                    <ul className="feature-lists">
                                        <li>Limitless concepts</li>
                                        <li>Community access</li>
                                        <li>Expert Reviews</li>
                                        <li>Premium Listing</li>
                                        <li>Direct Connections</li>
                                        <li>24/7 Support</li>
                                    </ul>
                                    <Link to="/#" className="theme-button"><FiShoppingCart className="icon" /> Get Started</Link>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel className="tabs-content text-center">
                            <div className="pricing-plan-grid">
                                <div className="pricing-plan-box">
                                    <h2 className="title">Basic</h2>
                                    <h1 className="price"> <sup>INR</sup>10000</h1>
                                    <h3 className="mo">/Year(save 20%)</h3>
                                    <ul className="feature-lists">
                                    <li>Limitless concepts</li>
                                        <li>Community access</li>
                                        <li>Expert Reviews</li>
                                        <li>Basic Listing</li>
                                        <li>Free Support</li>
                                        <li>Free registration</li>
                                    </ul>
                                    <Link to="/#" className="theme-button"><FiShoppingCart className="icon" /> Get Started</Link>
                                </div>
                                <div className="pricing-plan-box active">
                                    <h2 className="title">Popular</h2>
                                    <h1 className="price"> <sup>INR</sup>20000</h1>
                                    <h3 className="mo">/Year(save 20%)</h3>
                                    <ul className="feature-lists">
                                    <li>Limitless concepts</li>
                                        <li>Community access</li>
                                        <li>Expert Reviews</li>
                                        <li>Professional Listing</li>
                                        <li>Direct Connections</li>
                                        <li>24/7 Support</li>
                                    </ul>
                                    <Link to="/#" className="theme-button"><FiShoppingCart className="icon" /> Get Started</Link>
                                </div>
                                <div className="pricing-plan-box">
                                    <h2 className="title">Premium</h2>
                                    <h1 className="price"> <sup>INR</sup>30000</h1>
                                    <h3 className="mo">/Year(save 20%)</h3>
                                    <ul className="feature-lists">
                                    <li>Limitless concepts</li>
                                        <li>Community access</li>
                                        <li>Expert Reviews</li>
                                        <li>Premium Listing</li>
                                        <li>Direct Connections</li>
                                        <li>24/7 Support</li>
                                    </ul>
                                    <Link to="/#" className="theme-button"><FiShoppingCart className="icon" /> Get Started</Link>
                                </div>
                            </div>
                        </TabPanel>
                    </Tabs>
                </div>

            </section>
        )
    }
}
