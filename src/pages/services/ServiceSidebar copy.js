import React, { Component } from 'react'
// import ServiceCategory from './widgets/ServiceCategory'
// import ServiceContact from './widgets/ServiceContact'
// import ServiceBroucher from './widgets/ServiceBroucher'
// import ServiceSocial from './widgets/ServiceSocial'
import { Link } from 'react-router-dom'
import { FiChevronRight } from 'react-icons/fi'
import * as apiClient from "../../components/network/apiClient";

export const DisplayChildCategory = (props) => {
    let subCategories = props.category.subCategories;
    //console.log("subCategories = ");
    //console.log(subCategories);
    return (
        <>
            <ul className="sub-category">
                {subCategories && subCategories.map((item, index) => {
                    return (
                        <li key={index}>
                            <Link to="/service-details">
                                {item.serviceName}
                                <FiChevronRight className="icon" onClick={() => {
                                    props.onCategoryClick(item);
                                }} />
                            </Link>
                            {item.subCategories && <DisplayChildCategory onCategoryClick={props.onCategoryClick} category={item} />}
                        </li>
                    )
                })}
            </ul>
        </>
    )
}

export default class ServiceSidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            selectedCategory: {},
        };
    }
    loadSubCategories = (category) => {
        let { id } = category;
        if (category.subCategories) {
            return;
        }
        apiClient.getSubCategories(id).then(res => {
            let { categories } = this.state;
            let categoryArr = [...categories];
            let findIndex = categoryArr.findIndex(item => item.id == id);
            if (findIndex >= 0) {
                let cat = categoryArr[findIndex];
                cat.subCategories = res;
                this.setState({
                    categories: categoryArr
                });
            }
        }).catch(err => {
            console.log(err);
        });
    }
    componentDidMount() {
        apiClient.getTopLevelServiceCategories().then(res => {
            this.setState({
                categories: res
            });
        }).catch(err => {
            console.log(err);
        });
    }
    render() {
        const { categories } = this.state;
        return (
            <aside className="service-sidebar">
                <div className="service-widgets widget_categories">
                    <h3>Categories.</h3>
                    <ul>
                        {categories && categories.map((item, index) => {
                            return (
                                <li key={index}>
                                    <Link to="/service-details">
                                        {item.serviceName}
                                        <FiChevronRight className="icon" onClick={() => {
                                            this.loadSubCategories(item);
                                        }} />
                                    </Link>
                                    {item.subCategories && <DisplayChildCategory onCategoryClick={(item) => {
                                        this.loadSubCategories(item);
                                    }} category={item} />}
                                </li>
                            )
                        })}
                        {/* <li><Link to="/service-details">Accounting and Ancilliary Services <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Taxation Services<FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Corporate Legal Services <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">HR and Payroll Services <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Learning and Development <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Warehousing and Logistics <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Architect Services <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Strategy and Management <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Valuation and Investment <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Company Law Compliances <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">IT and Software Support <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Sales and Marketing <FiChevronRight className="icon" /></Link></li>
                        <li><Link to="/service-details">Real Estate <FiChevronRight className="icon" /></Link></li> */}
                    </ul>
                </div>

                {/* <ServiceCategory /> */}
                {/* <ServiceContact /> */}
                {/* <ServiceBroucher /> */}
                {/* <ServiceSocial /> */}
            </aside>
        )
    }
}
