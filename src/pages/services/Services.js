import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { FiTarget, FiMonitor } from "react-icons/fi";
import { MdAccountBalance } from "react-icons/md";
import { AiOutlineDollar, AiOutlineFileSearch } from "react-icons/ai";
import { IoIosBuild } from "react-icons/io";
import * as apiClient from "../network/apiClient";
import slider1 from '../../assets/images/img12.jpg';
import OwlCarousel from 'react-owl-carousel';
import '../../assets/css/owl.carousel.css';
import '../../assets/css/owl.theme.default.min.css';

export default class Service extends Component {
    constructor(props) {
        super(props);
        this.state = {
            services: []
        };
    }
    getServiceCategory = () => {
        apiClient.getServiceCategory(20).then(res => {
            //console.log(res);
            let items = res.map(item => {
                return {
                    icon: item.logo,
                    title: item.serviceName,
                    description: item.serviceDetails
                }
            });
            //console.log(items);
            this.setState({ services: items });
        }).catch(err => {
            console.log("Error : " + err);
        });
    }

    componentDidMount() {
        this.getServiceCategory();
    }
    // servicestates = {
    //     items: [
    //         {
    //             icon: <FiTarget className="service-items-icon" />,
    //             title: 'Accounting and Ancilliary Services.',
    //             description: 'Financial management may be defined as the area or function in an organization which is concerned with profitability, expenses, cash and credit, so that the organization may have the means to carry.'
    //         },
    //         {
    //             icon: <MdAccountBalance className="service-items-icon" />,
    //             title: 'Taxation Services.',
    //             description: 'In general, bank management refers to the process of managing the Bank statutory activity. Bank management is characterized by the specific object of management - financial relations connected with banking activities and other relations, also connected with implementation of management functions in banking.'
    //         },
    //         {
    //             icon: <AiOutlineDollar className="service-items-icon" />,
    //             title: 'Corporate Legal Services.',
    //             description: 'sed quia lipsum dolor sit atur adipiscing elit is nunc quis tellus sed ligula porta ultricies quis nec neulla.'
    //         },
    //         {
    //             icon: <AiOutlineFileSearch className="service-items-icon" />,
    //             title: 'HR and Payroll Services.',
    //             description: 'sed quia lipsum dolor sit atur adipiscing elit is nunc quis tellus sed ligula porta ultricies quis nec neulla.'
    //         },
    //         {
    //             icon: <IoIosBuild className="service-items-icon" />,
    //             title: 'Learning and Development',
    //             description: 'sed quia lipsum dolor sit atur adipiscing elit is nunc quis tellus sed ligula porta ultricies quis nec neulla.'
    //         },
    //         {
    //             icon: <FiMonitor className="service-items-icon" />,
    //             title: 'Warehousing and Logistics',
    //             description: 'sed quia lipsum dolor sit atur adipiscing elit is nunc quis tellus sed ligula porta ultricies quis nec neulla.'
    //         }
    //     ]
    // }
    render() {
        const { services } = this.state;
        return (
            <section className="services-area">
                <div className="container">

                    <div className="services-items-slider">
                        {services.length > 0 && <OwlCarousel className="owl-theme service-content-details-slider" loop={true} autoplay={false} margin={10} dots={false} nav={true} items={5} slideBy={4}>
                            {services.map((item, index) => {
                                return <div className="item">
                                    <img src={item.icon} className="service-category-logo" alt="Service Details Slider" />
                                    <div className="service-slider-details">
                                        <div className="service-items-title">{item.title}</div>
                                        <div className="slider-category-description">Category description</div>
                                    </div>

                                </div>
                            })}
                        </OwlCarousel>}
                    </div>
                    {/* 
                    <div className="services-items-grid">
                        {services && services.map((item, index) => {
                            return <div className="service-items" key={index}>
                                <span className="service-items-num">{index}</span>
                                <img src={item.icon} className="service-items-icon-image" />
                                <Link to="/service-details">
                                    <h3 className="service-items-title">{item.title}</h3>
                                </Link>
                                <p className="service-items-description">{item.description}</p>
                            </div>
                        })
                        }
                    </div> */}
                </div>
            </section>
        )
    }
}

