import React from 'react';

export default function Subscriptions({ data }) {
    return (
        <table className="detail-table">
            <thead>
                <tr>
                    <th>Subscription Name</th>
                    <th>Benifits   </th>
                    <th>Start Date  </th>
                    <th>End Date </th>
                </tr>
            </thead>
            { data && data.length > 0 && data.map((row, index) => (
                <tr key={ index }>
                    <td component="th" scope="row">
                        { row.name }
                    </td>
                    <td>{ row.alert_mobile_no }</td>
                    <td >{ row.session }</td>
                </tr>
            )) }
        </table>
    );
}
