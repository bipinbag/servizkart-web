import React, { Component } from 'react'
import Navbar from "../../../components/common/Navbar";
import BreadcrumbSmall from '../../../components/common/BreadcrumbSmall';
import UserLoginFormOTP from './LoginFormOTP';
import Footer from '../../blocks/Footer';
import login from "../../../assets/images/login.jpg";

export default class UserLoginPageOTP extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <>
                <header className="header-area">
                    <Navbar />
                </header>
                <BreadcrumbSmall title="Login" />
                <div className="page-banner">
                    <img src={ login } className="page-banner-image" />
                </div>
                <UserLoginFormOTP { ...this.props } />
                {/* <Footer /> */ }
            </>
        )
    }
}
