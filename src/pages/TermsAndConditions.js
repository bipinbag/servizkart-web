import React, { Component } from 'react'
import Navbar from '../components/common/Navbar'
import Breadcrumb from '../components/common/Breadcrumb'
import Pricing from '../components/other/Pricing'
import CtaForm from '../components/other/CtaForm'
import Footer from './blocks/Footer';

export default class TermsAndConditions extends Component {
    render() {
        return (
            <>
                <header className="header-area">
                    <Navbar />
                </header>
                <Breadcrumb title="Terms and Conditions" />
                <div className="container plain-pricing-table">
                    <div className="privacy-policy-container">
                        <p><strong>Terms and Conditions</strong>
                            The SERVIZKART Privacy Notice describes how SERVIZKART and its affiliates (collectively, "SERVIZKART") use your Personal Data in connection with SERVIZKART�s partner verification process. This notice supplements SERVIZKART�s Privacy Policy, which is available at www.serivizkart.com/privacy. Click <u>HERE </u>to review the Privacy Policy.<br />
                            <br />If you consent to SERVIZKART's collection, use, transfer, and sharing of your personal information as specified herein, please select �Yes� and click Next.</p>
                        <p>By declining to accept this Applicant Privacy Statement, SERVIZKART will not be able to process your application at this time.</p>
                        <p>Yes, I have read and consent to the terms and conditions of the privacy notice*</p>
                    </div>
                </div>
                <Footer />
            </>
        )
    }
}
