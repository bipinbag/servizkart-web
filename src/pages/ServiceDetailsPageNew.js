import React, { Component } from 'react';
import Navbar from '../components/common/Navbar';
import Breadcrumb from '../components/common/Breadcrumb';
import Divider from '../components/other/Divider';
import CtaForm from '../components/other/CtaForm';
import ServiceSidebar from './services/ServiceSidebar';
import ServiceDtlsContent from './services/ServiceDtlsContent';
import Footer from './blocks/Footer';
import SortableTree, { toggleExpandedForAll } from 'react-sortable-tree';
import 'react-sortable-tree/style.css';
import * as apiClient from "../components/network/apiClient";

export default class ServiceDetailsPageNEW extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            treeData: [],
        };
    }
    loadChildCategories = (category, allCategories) => {
        let allChildrens = allCategories.filter(i => i.parentId == category.id);
        allChildrens.forEach(cat => {
            let child = this.loadChildCategories(cat, allCategories);
            cat.title = cat.serviceName;
            cat.children = child;
        });
        return allChildrens;
    }
    loadServiceCategories = () => {
        apiClient.getServiceCategories().then(res => {
            //console.log(res);
            if (res.length > 0) {
                this.setState({ categories: res });
                let parentCategories = res.filter(item => item.parentId == 0);
                //console.log(parentCategories);
                parentCategories.forEach(cat => {
                    let child = this.loadChildCategories(cat, res);
                    cat.title = cat.serviceName;
                    cat.children = child;
                });
                //console.log(parentCategories);
                this.setState({
                    treeData: toggleExpandedForAll({
                        treeData: parentCategories,
                        expanded: true
                    })
                });
            }
        }).catch(err => {
            console.log("erro : " + err);
        });
    }

    componentDidMount() {
        this.loadServiceCategories();
    }
    render() {
        const { treeData } = this.state;
      //  console.log(treeData);
        return (
            <>
                <header className="header-area">
                    <Navbar />
                </header>
                <Breadcrumb title="Services" />
                <section className="service-details-wrapper">
                    <div className="container">
                        {treeData.length > 0 && <div className="service-details-grid ">
                            <SortableTree canDrag={false}
                                onChange={tD => this.setState({treeData : tD})}
                                treeData={treeData}
                                generateNodeProps={({ node, path }) => ({
                                    title: node.title,
                                    buttons: [
                                        <img alt="logo" className="category-logo" src={node.logo} />,
                                        <button
                                            onClick={() => {
                                                //console.log(node);
                                            }}>
                                            <i className="fa fa-edit"></i>
                                        </button>,
                                    ],
                                })}
                            />
                            {/* <ServiceSidebar />
                            <ServiceDtlsContent /> */}
                        </div>}
                    </div>
                </section>
                {/* <Divider />
                <div className="form-white-bg">
                    <CtaForm />
                </div> */}
                <Footer />
            </>
        )
    }
}
