import React, { Component } from 'react'
import { AiOutlineNotification, AiOutlineUsergroupAdd, AiOutlineCheckCircle } from 'react-icons/ai'
import { FaHandshake } from 'react-icons/fa'
import { FiChevronRight } from 'react-icons/fi'
import SectionsTitle from '../../components/common/SectionsTitle';
import { Link } from 'react-router-dom'
import CountUp from 'react-countup';
import * as apiClient from "../../components/network/apiClient";

export default class NumberSpeak extends Component {
    constructor(props) {
        super(props);
        this.state = {
            counterstates: {
                items: []
            },
        };
    }

    getCounterData = async () => {
        let NumberSpeaksProjectCount = await apiClient.getContentPageConfig("NumberSpeaks-ProjectCount");
        let NumberSpeaksTeamCount = await apiClient.getContentPageConfig("NumberSpeaks-TeamCount");
        let NumberSpeaksClientCount = await apiClient.getContentPageConfig("NumberSpeaks-ClientCount");
        let NumberSpeaksExperienceCount = await apiClient.getContentPageConfig("NumberSpeaks-ExperienceCount");
        let ThisYearCompanyCount = await apiClient.getContentPageConfig("ThisYear-CompanyCount");
        let count1 = NumberSpeaksProjectCount[0].propertyName;
        let count2 = NumberSpeaksTeamCount[0].propertyName;
        let count3 = NumberSpeaksClientCount[0].propertyName;
        let count4 = NumberSpeaksExperienceCount[0].propertyName;
        let count5 = ThisYearCompanyCount[0].propertyName;

        this.setState({
            counterstates: {
                items: [
                    {
                        icon: <AiOutlineNotification className="icon" />,
                        number: <CountUp start={0}
                            end={count1}
                            duration={8}
                            separator=" "
                            decimal=","
                            prefix=" "
                            suffix=" "
                        />,
                        numsuffix: '+',
                        title: 'Finishing Projects'
                    },
                    {
                        icon: <AiOutlineUsergroupAdd className="icon" />,
                        number: <CountUp start={0}
                            end={count2}
                            duration={8}
                            separator=" "
                            decimal=","
                            prefix=" "
                            suffix=" "
                        />,
                        numsuffix: '+',
                        title: 'Team Members'
                    },
                    {
                        icon: <FaHandshake className="icon" />,
                        number: <CountUp start={0}
                            end={count3}
                            duration={8}
                            separator=" "
                            decimal=","
                            prefix=" "
                            suffix=" "
                        />,
                        numsuffix: '+',
                        title: 'Lovely Clients'
                    },
                    {
                        icon: <AiOutlineCheckCircle className="icon" />,
                        number: <CountUp start={0}
                            end={count4}
                            duration={8}
                            separator=" "
                            decimal=","
                            prefix=" "
                            suffix=" "
                        />,
                        numsuffix: '+',
                        title: 'Years Of Experience'
                    }
                ]
            }
        }
        );
    }

    componentDidMount() {
        this.getCounterData();
    }
    render() {
        const { counterstates } = this.state;
        return (
            <>
                <section className="counterup-wrapper text-center">
                    <div className="container">
                        <SectionsTitle title="Numbers Speak." subtitle="See What Our" />
                        <div className="counterup-grid-wrap">

                            {counterstates.items.map((item, index) => {
                                return <div className="counterup-items" key={index}>
                                    <div className="counterup-iconbox">
                                        {item.icon}
                                    </div>
                                    <h1 className="counterup-number">{item.number}{item.numsuffix}</h1>
                                    <p className="counterup-title">{item.title}</p>
                                </div>
                            })}

                        </div>
                        <div className="counterup-bottom-wrap">
                            <p>Do not hesitate to contact us for instant help and support</p>
                            <Link to="/#" className="theme-button">Let's Start Now <FiChevronRight className="icon" /></Link>
                        </div>
                    </div>
                </section>
            </>
        )
    }
}
