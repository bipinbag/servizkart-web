
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { FiChevronRight, FiPlay } from 'react-icons/fi'
import about_video_img from '../../assets/images/img1.jpg'
import history_img from '../../assets/images/img2.jpg'
import ModalVideo from 'react-modal-video'
import * as apiClient from "../../components/network/apiClient";

export default class HomeTrust extends Component {
    constructor() {
        super()
        this.state = {
            isOpen: false,
            trustBanner: about_video_img,
            trustBannerHeading: '<h1 className="about-left-title">We Are Trusted by More Than <strong>100 Clients.</strong> </h1>',
            trustBannerVideo: "",
            videoCaption: "Watch Video",
        }
        this.openModal2 = this.openModal2.bind(this)
    }

    openModal2() {
        this.setState({ isOpen: true })
    }

    componentDidMount() {
        apiClient.getContentPageConfig("TrustBanner").then(res => {
            if (res.length > 0) {
                this.setState({ trustBannerHeading: res[0].propertyName, trustBanner: res[0].imageUrl });
            }

        });
        apiClient.getContentPageConfig("TrustBannerVideo").then(res => {
            if (res.length > 0) {
                this.setState({ videoCaption: res[0].propertyName, trustBannerVideo: res[0].imageUrl });
            }
        });

    }
    render() {
        const { trustBanner, trustBannerVideo, trustBannerHeading, videoCaption } = this.state;
        return (
            <section className="about-st1-wrapper">
                <div className="container">
                    <div className="about-st1-grid">
                        <div className="about-left" dangerouslySetInnerHTML={{ __html: trustBannerHeading }}>
                        </div>
                        <div className="about-right">
                            <img className="about-right-img" src={trustBanner} alt="About Video" />
                            <ModalVideo channel='youtube' isOpen={this.state.isOpen} videoId='t3tsMEhlvwM' onClose={() => this.setState({ isOpen: false })} />
                            <div className="video-button-box">
                                <Link to="/#" className="video-button" onClick={this.openModal2}>
                                    <span className="icon"><FiPlay /></span>
                                    <span dangerouslySetInnerHTML={{ __html: videoCaption }}></span>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
