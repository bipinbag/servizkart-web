import React from 'react'
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { FiPlus, FiChevronRight } from 'react-icons/fi'
import { FaRegComment } from 'react-icons/fa'

export default function AppPartner({ partner }) {
    const { name, image, contactFirstName, contactLastName, contactEmail, description, emailAddress, webSite } = partner;

    return (
        <div className="blog">
            <div className="blog-img-box">
                <img src={image} alt="Blog" />
                {/* <div className="blog-overlay">
                    <Link to="/blog-details" ><FiPlus className="icon" /></Link>
                </div> */}
            </div>
            <div className="blog-content-box">
                <h2 className="blog-title">
                    {name}
                </h2>
                <div className="partner-item">
                    <span className="blog-meta-name">Contact : </span>
                    <span className="blog-meta-value">{contactFirstName} {contactLastName}</span>
                </div>
                <div className="partner-item">
                    <span className="blog-meta-name">Email : </span>
                    <span className="blog-meta-value">{contactEmail}</span>
                </div>
                <div className="partner-item">
                    <span className="blog-meta-name">Website : </span>
                    <span className="blog-meta-value">{emailAddress}</span>
                </div>
                {/* <Link to="/blog-details">
                    <h2 className="blog-title">
                        {name}
                    </h2>
                </Link> */}
                <p className="blog-desc">
                    {description}
                </p>
                {/* <div className="blog-footer">
                    <Link className="theme-button" to="/blog-details">{button} <FiChevronRight className="icon" /></Link>
                    <div className="blog-comment">
                        <Link to="/blog-details">{commentnum} <FaRegComment className="comment" /></Link>
                    </div>
                </div> */}
            </div>
        </div>

    )
}

